import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { PostsService } from './services/posts.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  // Se Trabaja con una fake API REST para pruebas de peticiones.
  // Sus instrucciones de uso estan en la sig url: https://jsonplaceholder.typicode.com/guide/
  
  arrPosts: any[];
  formulario: FormGroup;

  constructor(private postService: PostsService) {
    this.formulario = new FormGroup({
      title: new FormControl(''),
      body: new FormControl(''),
      userId: new FormControl('')
    })
  }
  
  ngOnInit(){
    this.postService.getAll()
    .then(posts=> this.arrPosts = posts)
    .catch(error => console.log(error));
  }

  async onClick(postId) {
    try {
      const post = await this.postService.getById(postId); 
      console.log(post);
    } catch (error) {
      console.log(error);
    }
  }

  onClickPost(){
    this.postService.create({
      title: 'Nuevo titulo',
      body: 'Este es el cuerpo del post',
      userId: 1
    }).then(response => console.log(response))
      .catch(error => console.log(error))
  }

  async onSubmit() {
    try{
      const response = await this.postService.create(this.formulario.value);
      console.log(response);
    }catch (error){
      console.log(error);
    }
  }

  onClickUpdate(){
    this.postService.upadate({
      id: 5,
      title: 'Nuevo titulo',
      body: 'Nuevo cuerpo para el post',
      userId: 3
    }).then(response => console.log(response))
      .catch(error => console.log(error))
  }

  async onClickDelete(){
    try{
      const response = await this.postService.delete(5);
      console.log(response);
    }catch (error) {
      console.log(error);
    }
  }
}
